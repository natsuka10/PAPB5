package com.natsu.papb5


import android.annotation.SuppressLint
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.natsu.papb5.data.AppDatabase
import com.natsu.papb5.data.entity.User


class MainActivity : AppCompatActivity() {
    private lateinit var database: AppDatabase
    private lateinit var adapter: AdapterItems
    private var list = mutableListOf<User>()
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val nameInput = findViewById<EditText>(R.id.tfName)
        val nimInput = findViewById<EditText>(R.id.tfNim)
        val addBtn = findViewById<Button>(R.id.button)
        //Recycle View
        recyclerView = findViewById<RecyclerView>(R.id.recyclerHero)
        database = AppDatabase.getInstance(applicationContext)
        adapter = AdapterItems(list)
        adapter.setDialog(object : AdapterItems.Dialog{

            //Menghapus data tertentu dengan menekan view item tersebut
            override fun onClick(position: Int) {
                val dialog = AlertDialog.Builder(this@MainActivity)
                dialog.setTitle( list[position].nama +" Hapus?")
                dialog.setItems(R.array.pilihan, DialogInterface.OnClickListener{ dialog,
                                                                                       witch ->
                    if (witch==0){
                        // Hapus Data
                        Toast.makeText(applicationContext, list[position].nama + " Berhasil dihapus", Toast.LENGTH_SHORT).show()
                        database.userDao().delete(list[position])
                        getData()
                    }else{
                        //Batal hapus
                        dialog.dismiss()
                    }
                })

                //menampilkan dialog
                val dialogView = dialog.create()
                dialogView.show()
            }

        })

        recyclerView.layoutManager = LinearLayoutManager(applicationContext, VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(applicationContext, VERTICAL))
//        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter

        addBtn.setOnClickListener {
            val nama = nameInput.text.toString()
            val nim = nimInput.text.toString()

            if (nama.isNotEmpty() && nim.isNotEmpty()){
                database.userDao().insertAll(User(
                    null,
                    nama,
                    nim
                ))
                Toast.makeText(applicationContext, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(applicationContext, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show()
            }

            nameInput.text.clear()
            nimInput.text.clear()

            //Hide softkeyboard after submit
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(nameInput!!.windowToken, 0)

            getData()

        }

    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun getData(){
        list.clear()
        list.addAll(database.userDao().getAll())
        adapter.notifyDataSetChanged()
    }
}