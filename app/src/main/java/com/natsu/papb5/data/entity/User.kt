package com.natsu.papb5.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = true) var id: Int? = null,
    @ColumnInfo(name = "Nama") var nama: String?,
    @ColumnInfo(name = "NIM") var nim: String?

)
